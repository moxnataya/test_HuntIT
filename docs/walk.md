# Как выгулять собаку

Эта инструкция о том, как погулять с собакой: от подготовки и до возвращения домой. Выгул щенка и взрослой собаки может отличаться: здесь мы опишем прогулку со взрослой собакой.

## Подготовка

### Как понять, что пора гулять

Идем на прогулку, если:

1. Пришло время регулярной прогулки: утренней или вечерней.
2. Собака своим поведением дает понять, что хочет гулять.

**Основные сигналы, что собака хочет гулять:**

* собака беспокойна: крутится, «не неходит себе места»;
* собака подходит к вам, смотрит в глаза, садится рядом;
* собака садится у двери или подскуливает.

### Подготовка собаки
Сперва подговьте амуницию и аксессуары — это очень важно.

 **Вот минимальный прогулочный набор**: 

* ошейник/шлейка,
* поводок,
* игрушка,
* лакомства,
* в теплую погоду — вода с поильником.

Если собака подбирает с земли, возьмите намордник. <br> В дождливую погоду наденьте на собаку дождевик или комбенизон.

**Лайфхаки:**

- Выбирайте поводок длиной не менее 3-5 метров — так собаке комфортно исследовать среду, и она меньше тянет.
- Выбирайте игрушку, которая поместится в карман, так как большую часть прогулки вам придется носить ее самому.
- Для прогулок выбирайте мелкие сухие лакомства, «на один укус».
- В жаркую погоду в бутылку с водой можно добавить пару кубиков льда, чтобы вода дольше оставалась прохладной.

### Подготовка себя
**А вот как самому подготовиться к прогулке:**

- Выбирайте одежду, которую нежалко испачкать или даже порвать.
- Выбирайте удобную закрытую обувь, чтобы можно было ходить не только по асфальту.
- В дождь лучше надеть дождевик — одновременно вести собаку и держать зонт очень неудобно.

!!! tip
    Вначале оденьтесь сами, а ошейник или шлейку надевайте на пса в последнюю очередь. В противном случае собака может перевозбудиться еще до выхода из дома.

## На улице

Выводите собаку на улицу с пристегнутым поводком. Это важно для ее безопасности. <br>

1. Сразу после выхода из дома собаке нужно в туалет: несколько раз пописать и покакать.


2. После этого можно идти на собачью площадку и немного позаниматься. Здесь вам пригодятся лакомства: их можно давать за выполнение команд или трюков. Еще можно сходить в парк или сквер. 

После нескольких прогулок вы уже будете знать, где и в какое время собираются гуляющие с собаками, чтобы гулять вместе и помогать собаке социализироваться. Или, наоборот, чтобы избегать собачьего часа-пик, если собака не очень любит активное общение с сородичами. 

Во время прогулки чередуйте циклы активности и отдыха: постоянные игры перегрузят нервную систему пса, а постоянное лежание не отвечает потребностям собаки в активности и исследовании среды.

Прогулка с собакой занимает час-полтора. На постоянной основе гулять меньше или больше не рекомендуется: слишком коротких прогулок собаке недостаточно, а слишком длинные приведут к сильному перевозбуждению в долгой перспективе. 

!!! Важно
     Перед возвращением домой летом проверьте собаку на клещей: они могут быть на шерсти или на коже.


## Возвращение домой

Чаще всего после прогулки придется мыть собаке лапы:

<ol start="1"> 
1. У входной двери проверьте, насколько грязные лапы у собаки. Если очень грязные, протрите их специальной влажной салфеткой для собак. Если лапы не очень грязные или ванная комната близко от входа, можно сразу быстро довести собаку в ванную.
</ol>
!!! tip
    Дождевик или комбенизон лучше снять перед входом в квартиру, чтобы собака не испачкала стены и мебель. 

<ol start="2">
2. В ванной комнате собаку поместите собаку в ванную или заведите в душевую кабину. Для мытья лап пользуйтесь специальными шампунями для животных. Ветеринары рекомендуют помещать собаку в ванную и доставать из нее на руках — это поможет избежать травм.
</ol>
<ol start="3">
3. После мытья обязательно просушите лапы собаки полотенцем, чтобы питомец не скользил по полу. А у особо шерстяных собак от влаги может образоваться грибок. 
</ol>

После мытья лап покормите собаку. Если у вас другой график кормления, дайте животному отдохнуть: после возвращения с прогулки у собак наступает время отдыха и сна, лучше их не тревожить. <br> 

Повторим процес в виде схемы:
![Как перевести деньги в приложении](images/walkthedog.svg)