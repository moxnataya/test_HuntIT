# Перевод на карту в мобильном приложении Т-Банка

С помощью этой инструкции вы сможете перевести деньги на карту другому человеку. Чтобы сделать такой перевод, вы уже должны быть клиентом **Т-Банка**. Если у вас еще нет счета, перейдите по <a href="https://www.tbank.ru/cards/debit-cards/" target="_blank">ссылке</a>, чтобы его открыть.

Вот пошаговая схема, как делать перевод:
![Как перевести деньги в приложении](images/diagram1.svg)

## Как сделать перевод

1. Установите приложение **Т-Банка**. [Инструкции для скачивания приложений для Android и iPhone](https://www.tinkoff.ru/apps/#iOS). <br>
При первом входе вам нужно будет задать пин-код.
2. На главной странице приложения выберите **Платежи**.
<div class="content">
    <img src="images/first screen.jpg" alt="Начальный экран приложения" class="medium-img" onclick="openModal('myModal')" style="width: 200px; cursor: pointer;"/>
</div>
<!-- Модальное окно -->
<div id="myModal" class="modal">
  <span class="close" onclick="closeModal('myModal')">&times;</span>
  <div class="modal-content">
    <img class="mySlides" src="images/first screen.jpg" style="width:100%">
  </div>
</div>
<ol start="3"> 
3. Нажмите на панель <b>По номеру телефона</b>. <br>
</ol>
<div class="content">
    <img src="images/vyberite po nomery karty.jpg" alt="Переход к переводам" class="medium-img" onclick="openModal('myModal3')" style="width: 200px; cursor: pointer;"/>
</div>
<!-- Модальное окно -->
<div id="myModal3" class="modal">
  <span class="close" onclick="closeModal('myModal3')">&times;</span>
  <div class="modal-content">
    <img class="mySlides" src="images/vyberite po nomery karty.jpg" style="width:100%">
  </div>
</div>


<ol start="4">
  <li>В открывшемся разделе выберите счет, с которого вы хотите перевести деньги. Затем введите номер карты и сумму перевода. </li>
</ol>
> Если карта, на которую вы будете делать перевод, рядом, вы можете отсканировать ее, а не вводить номер вручную.  

<div class="content">
    <img src="images/nomer karty.jpg" alt="Перевод по номеру карты" class="medium-img" onclick="openModal('myModal2')" style="width: 200px; cursor: pointer;"/>
</div>
</details>
<!-- Модальное окно -->
<div id="myModal2" class="modal">
  <span class="close" onclick="closeModal('myModal2')">&times;</span>
  <div class="modal-content">
    <img class="mySlides" src="images/nomer karty.jpg" style="width:100%">
  </div>
</div>
<br>

<ol start="5">
5. Проверьте введенные данные, сумму комиссии и нажмите <a href="http://127.0.0.1:8000/images/transaction%20done.jpg" class="button-like">Перевести</a>. Если перевод прошел, вы перейдете на экран завершения платежа. <br><br>

<style>
.button-like {
    padding: 4px 8px;
    background-color:#ffdc01;
    color: #fff;
    border: 1px solid #ffdc01;
    border-radius: 4px;
    cursor: pointer;
    text-decoration: none; /* Отменяет подчеркивание */
}
</style>

<div class="content">
    <img src="images/transaction done.jpg" alt="Завершение транзакции" class="medium-img" onclick="openModal('myModal1')" style="width: 200px; cursor: pointer;"/>
</div>
</details>
<!-- Модальное окно -->
<div id="myModal1" class="modal">
  <span class="close" onclick="closeModal('myModal1')">&times;</span>
  <div class="modal-content">
    <img class="mySlides" src="images/transaction done.jpg" style="width:100%">
  </div>
</div>
<br>
